# IoT Project - Socket.IO Chat

## Supervisor
- Prof. Yérom-David BROMBERG
- MSc. Chung Quang Khanh


## Student Information
- Student Name: Trần Thanh Huy
- Mobile Phone: 0886.363.368
- Email       : huytran190194@gmail.com


## Requirement:

| Type | Title |
| :------------ |:---------------|
|       [TD1](requirement/td1.md)       |    Basic websocket chat application|
|       [TD2](requirement/td2.md)       |    Advanced websocket chat application|
|       [TD3](requirement/td3.md)       |    Testing|
|       [TD4](requirement/td4.md)       |    Secure websocket chat application|


## Report
- [Microsoft Word Format]([IoT]Project_Report_TranThanhHuy.docx)
- [PDF Format]([IoT]Project_Report_TranThanhHuy.pdf)