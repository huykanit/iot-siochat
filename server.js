/*
    The custom event name: packet

    Response in JSON format:
    {sender: name, action: "join", password: what}
    {sender: name, action: "broadcast", msg: msg_content}
    {sender: name, action: "list", users: list of users}
    {sender: name, action: "quit"}

    {sender: name, action: "send", dest: whom, msg: msg_content}
    {sender: name, action: "cgroup", group: name}
    {sender: name, action: "jgroup", group: name}
    {sender: name, action: "gbroadcast", group: name, msg: msg_content}
    {sender: name, action: "members", group: name, members: list of members}
    {sender: name, action: "msgs", group: name, msgs: the history of messages}
    {sender: name, action: "umsgs", user: name, msgs: the history of messages}
    {sender: name, action: "groups", groups: list of groups}
    {sender: name, action: "leave", group: name}
    {sender: name, action: "invite", group: name, dest: whom}
    {sender: name, action: "kick", group: name, dest: whom, reason: reason}
    {sender: name, action: "ban", group: name, dest: whom, reason: reason}
    {sender: name, action: "unban", group: name, dest: whom}

    The first step of establishing secure connection
    {sender: name, action: "secure_1", dest: whom, public_key: pub_key}

    The second step
    {sender: name, action: "secure_2", dest: whom, public_key: pub_key}
    {sender: name, action: "secure_send", dest: whom, encrypted_msg: what, iv: what}
*/

const port = 3000;
const io = require('socket.io')(port); /* Load 'socket.io' module */

/* Load 'elasticsearch' module */
const elasticsearch = require('@elastic/elasticsearch');
const es_client = new elasticsearch.Client({ node: 'http://localhost:9200' });

/* Load bcrypt module */
const bcrypt = require('bcrypt');
const saltRounds = 10;

console.log("Server is listening on port: %d", port);

/* Listen to the 'connect' event that fired upon a successfull connection */
io.on('connect', (socket) => {
    console.log("A user connected");

    /* Listen to 'disconnect' event that fired upon a successfull disconnection */
    socket.on('disconnect', (reason) => {
        console.log("A user disconnected, reason: %s", reason);
        delete io.of('/').sockets[socket.id];
        console.log("Number of users: %d", io.of('/').server.engine.clientsCount);
    });

    /* Listen to 'packet' event from an individual socket */
    socket.on('packet', (packet_data) => {
        switch (packet_data.action) {
            case "join":
                console.log("Nickname: ", packet_data.sender, ", ID: ", socket.id);
                console.log("Number of users: %d", io.of('/').server.engine.clientsCount);

                /* Check if a nickname exists */
                /*  index: accounts
                    documents in the index:
                        { "nickname": "ted", "password": "hi" }
                        { "nickname": "roni", "password": "hi" }
                */
                es_client.search({
                    index: "accounts",

                    /* Search queries */
                    body: {
                        "query": {
                            "match": {
                                "nickname": packet_data.sender
                            }
                        }
                    }
                }, (err, result) => {
                    if (err) console.log(err);
                    if (404 === result.statusCode ||
                        /* Index not found, this means this is the first time that a user join,
                            so just save the account information */

                        (200 === result.statusCode && null === result.body.hits.max_score)) {
                        /* Nickname not found, this means this is the first time that a user join,
                            so just save the account information */

                        if (404 === result.statusCode){
                            console.log("Index not found!"); 
                        }
                        else { console.log("Nickname not found!"); }

                        bcrypt.hash(packet_data.password, saltRounds, function(err, hashed_password) {
                            es_client.create({
                                id: new Date().getTime(),
                                index: "accounts",
                                refresh: 'true',
                                body: {
                                    "nickname": packet_data.sender,
                                    "password": hashed_password
                                }
                            }, (err, result) => {
                                if (err) console.log(err);
                                if (201 === result.statusCode) {
                                    console.log("Saved the account to the database");
                                }
                            });
                        });

                    } else {
                        /* Found the nickname, check if the password matches or not */
                        bcrypt.compare(packet_data.password, result.body.hits.hits[0]._source.password,
                                        function(err, result) {
                            if (true === result) {
                                console.log("Correct password");

                                /*  Broadcasting means sending a packet to everyone else
                                except for the socket that starts it */
                                socket.broadcast.emit('packet', {sender: packet_data.sender, action: packet_data.action});
                            } else {
                                console.log("Incorrect password!");
                                socket.disconnect(true);
                            }
                        });
                    }
                });

                /* Save nickname to this socket object */
                socket.nickname = packet_data.sender;
            break;

            case "broadcast":
                /*  Broadcasting means sending a packet to everyone else
                except for the socket that starts it */
                socket.broadcast.emit('packet', packet_data);

                /* Save all messages to elasticsearch
                    index: room_messages
                    documents in the index:
                        { "group": socket.id, "sender": ted, "msg": "hi" }
                        { "group": socket.id, "sender": roni, "msg": "hi" }
                */
                /* Adds a JSON document to the specified index and makes it searchable.
                If the document already exists, updates the document and increments its version.*/
                es_client.create({
                    /* Document ID */
                    id: new Date().getTime(),

                    /* By default, the index is created automatically if it doesn’t exist */
                    index: "room_messages",

                    /* If true then refresh the affected shards to make this operation visible
                    to search */
                    refresh: 'true',

                    /* Document data */
                    body: {
                        "group": socket.id,
                        "sender": packet_data.sender,
                        "msg": packet_data.msg
                    }
                }, (err, result) => {
                    if (err) console.log(err);
                    if (201 === result.statusCode) {
                        console.log("Saved the message to the database");
                    }
                });
            break;

            case "list":
                var users = [];

                /* Search all nicknames */
                for (var key in io.of('/').sockets) {
                    users.push(io.of('/').sockets[key].nickname);
                }

                /* Sending a packet to socket that starts it */
                socket.emit('packet', {"sender": packet_data.sender,
                                        "action": "list",
                                        "users": users});
            break;

            case "quit":
                /*  Broadcasting means sending a packet to everyone else
                except for the socket that starts it */
                socket.broadcast.emit('packet', packet_data);
                socket.disconnect(true);
            break;

            case "send":
                var socket_id = null;

                /* Search all nicknames */
                for (var key in io.of('/').sockets) {
                    if (packet_data.dest.toLowerCase() === io.of('/').sockets[key].nickname) {
                        socket_id = io.of('/').sockets[key].id;
                    }
                }

                if (socket_id !== null) {
                    /* Each socket is identified by a random,
                    unguessable, unique identifier Socket#id. For your convenience,
                    each socket automatically joins a room identified by its own id. */
                    io.to(socket_id).emit('packet', packet_data);

                    /* Save all messages to elasticsearch
                    index: room_messages
                    documents in the index:
                        { "group": socket.id, "sender": ted, "msg": "hi" }
                        { "group": socket.id, "sender": roni, "msg": "hi" }
                    */
                    /* Adds a JSON document to the specified index and makes it searchable.
                    If the document already exists, updates the document and increments its version.*/
                    es_client.create({
                        /* Document ID */
                        id: new Date().getTime(),

                        /* By default, the index is created automatically if it doesn’t exist */
                        index: "room_messages",

                        /* If true then refresh the affected shards to make this operation visible
                        to search */
                        refresh: 'true',

                        /* Document data */
                        body: {
                            "group": socket.id,
                            "sender": packet_data.sender,
                            "msg": packet_data.msg
                        }
                    }, (err, result) => {
                        if (err) console.log(err);
                        if (201 === result.statusCode) {
                            console.log("Saved the message to the database");
                        }
                    });
                }
            break;

            /* Create a group: cg;friends */
            case "cgroup":
                // console.log("need?");
                socket.emit('packet', packet_data);
            break;

            /* Join a group: j;friends */
            case "jgroup":
                socket.join(packet_data.group, () => {
                    console.log("Group: ", packet_data.group, ", Joined: ", packet_data.sender);

                    /* Sending to all clients in a group, including sender */
                    // io.to(packet_data.group).emit('packet', packet_data);

                    /* Sending to all clients in a group except sender */
                    socket.to(packet_data.group).emit('packet', packet_data);
                });
            break;

            /* Broadcast a message to a group: bg;friends;hello */
            case "gbroadcast":
                /* Sending to all clients in a group except sender */
                socket.to(packet_data.group).emit('packet', packet_data);

                /* Save all messages belonging a group to elasticsearch
                    index: room_messages
                    documents in the index:
                        { "group": friends, "sender": ted, "msg": "hi" }
                        { "group": friends, "sender": roni, "msg": "hi" }
                */
                /* Adds a JSON document to the specified index and makes it searchable.
                If the document already exists, updates the document and increments its version.*/
                es_client.create({
                    /* Document ID */
                    id: new Date().getTime(),

                    /* By default, the index is created automatically if it doesn’t exist */
                    index: "room_messages",

                    /* If true then refresh the affected shards to make this operation visible
                    to search */
                    refresh: 'true',

                    /* Document data */
                    body: {
                        "group": packet_data.group,
                        "sender": packet_data.sender,
                        "msg": packet_data.msg
                    }
                }, (err, result) => {
                    if (err) console.log(err);
                    if (201 === result.statusCode) {
                        console.log("Saved the message to the database");
                    }
                });
            break;

            /* List all clients that are inside a group: members;friends */
            case "members":
                var members = [];

                io.of('/').in(packet_data.group).clients((error, clients) => {
                    if (error) throw error;

                    /* clients are an array of socket ids in this group */
                    for (var i = clients.length - 1; i >= 0; i--) {
                        members.push(io.of('/').sockets[clients[i]].nickname);
                    }

                    /* Sending a packet to socket that starts it */
                    socket.emit('packet', {"sender": packet_data.sender,
                                            "action": "members",
                                            "group": packet_data.group,
                                            "members": members});
                });
            break;

            /* List the history of messages exchanged in a group:
            messages;friends */
            case "msgs":
                var msgs = [];
                /*  index: room_messages
                    documents in the index:
                        { "group": friends, "sender": ted, "msg": "hi" }
                        { "group": friends, "sender": roni, "msg": "hi" }
                */
                es_client.search({
                    index: "room_messages",

                    /* Search queries */
                    body: {
                        "query": {
                            "match": {
                                "group": packet_data.group
                            }
                        }
                    }
                }, (err, result) => {
                    if (err) console.log(err);
                    console.log(result);
                    for (var i = 0; i < result.body.hits.hits.length; i++) {
                        msgs.push(result.body.hits.hits[i]._source.sender + ": " +
                            result.body.hits.hits[i]._source.msg);
                    }

                    /* Sending a packet to socket that starts it */
                    socket.emit('packet', {"sender": packet_data.sender,
                                            "action": "msgs",
                                            "group": packet_data.group,
                                            "msgs": msgs});
                });
            break;

            /* List the history of messages belonging to a user:
            umessages;ted */
            case "umsgs":
                var msgs = [];
                /*  index: room_messages
                    documents in the index:
                        { "group": friends, "sender": ted, "msg": "hi" }
                        { "group": friends, "sender": roni, "msg": "hi" }
                */
                es_client.search({
                    index: "room_messages",

                    /* Search queries */
                    body: {
                        "query": {
                            "match": {
                                "sender": packet_data.user
                            }
                        }
                    }
                }, (err, result) => {
                    if (err) console.log(err);

                    for (var i = 0; i < result.body.hits.hits.length; i++) {
                        msgs.push(result.body.hits.hits[i]._source.group + ": " +
                            result.body.hits.hits[i]._source.msg);
                    }

                    /* Sending a packet to socket that starts it */
                    socket.emit('packet', {"sender": packet_data.sender,
                                            "action": "umsgs",
                                            "user": packet_data.user,
                                            "msgs": msgs});
                });
            break;

            /* List the existing groups: groups; */
            case "groups":
                const rooms = io.of('/').adapter.rooms;

                /* Sending a packet to socket that starts it */
                socket.emit('packet', {"sender": packet_data.sender,
                                        "action": "groups",
                                        "groups": Object.keys(rooms)});

            break;
            
            /* Leave a group: leave;friends */
            case "leave":
                socket.leave(packet_data.group, () => {
                    console.log("Group: ", packet_data.group, ", Left: ", packet_data.sender);

                    /* Sending to all clients in a group, including sender */
                    io.to(packet_data.group).emit('packet', packet_data);
                });
            break;

            case "invite":
                console.log("[INFO] Invite user <%s> to group <%s>", packet_data.dest, packet_data.group);
                var socket_id = null;
                for(var key in io.of("/").sockets){
                    if (io.of("/").sockets[key].nickname !== undefined && packet_data.dest.toLowerCase() === io.of("/").sockets[key].nickname.toLowerCase()) {
                        socket_id = io.of("/").sockets[key].id;
                    }
                }

                if (socket_id != null) {
                    console.log(socket_id);
                    io.sockets.connected[socket_id].join(packet_data.group);
                    io.to(packet_data.group).emit('packet', packet_data);
                }
                break;

            case "kick":
                console.log("[INFO] %s kick user %s out of group %s", packet_data.sender, packet_data.dest, packet_data.group);
                var socket_id = null;
                for(var key in io.of("/").sockets){
                    if (packet_data.dest.toLowerCase() === io.of("/").sockets[key].nickname.toLowerCase()) {
                        socket_id = io.of("/").sockets[key].id;
                    }
                }

                if (socket_id != null) {
                    // console.log(socket_id);
                    io.sockets.connected[socket_id].leave(packet_data.group, () => {
                        console.log("[INFO] User %s left the group %s", packet_data.dest, packet_data.group);
                        io.to(packet_data.group).emit('packet', packet_data);
                    });
                }
            break;
            
            case "states":
                console.log("[INFO] List states of group %s", packet_data.group);
                break;
            
            case "ban":
                console.log("[INFO] Ban user %s of group %s", packet_data.dest, packet_data.group);
                
                es_client.search({
                    index: "ban_accounts",

                    /* Search queries */
                    body: {
                        "query": {
                            "bool": {
                                "must": [
                                    {"match": {"group": packet_data.group}},
                                    {"match": {"user": packet_data.dest}}
                                ]
                            }
                        }
                        // "query": {
                        //     "match": { "user": packet_data.dest}
                        // }
                    }
                }, (err, result) => {
                    if (err) console.log(err);
                    if (404 === result.statusCode || 400 === result.statusCode || 
                        (200 === result.statusCode && null === result.body.hits.max_score)) {
                        if (404 === result.statusCode){
                            console.log("[ERROR] Index not found!"); 
                        }
                        else { console.log("[ERROR] Group or User not found!"); }
                
                        es_client.create({
                            id: new Date().getTime(),
                            index: "ban_accounts",
                            refresh: 'true',
                            body: {
                                "group": packet_data.group,
                                "user": packet_data.dest
                            }
                        }, (err, result) => {
                            if (err) console.log(err);
                            if (201 === result.statusCode) {
                                console.log("[INFO] Saved the ban account to the database");
                                io.to(packet_data.group).emit('packet', packet_data);
                            }
                        });
                    } else {
                        console.log("[INFO] This user already ban!");
                    }
                });
                break;

            case "unban":
                console.log("[INFO] Unban user %s of group %s", packet_data.dest, packet_data.group);
                es_client.deleteByQuery({
                    index: "ban_accounts",
                    body: {
                        "query": {
                            "bool": {
                                "must": [
                                    {"match": {"group": packet_data.group}},
                                    {"match": {"user": packet_data.dest}}
                                ]
                            }
                        }
                    }
                }, (err, result) => {
                    if (err) console.log(err);
                    if (404 === result.statusCode || 400 === result.statusCode){
                        if (404 === result.statusCode){
                            console.log("[ERROR] Index not found!"); 
                        }
                        else { console.log("[ERROR] Group or User not found!"); }
                    } else if (200 === result.statusCode) {
                        console.log("[INFO] %s has been unban!", packet_data.dest);
                    }
                });
                break;

            case "secure_1":
                var socket_id = null;

                console.log("[%s]: Public key: %s", packet_data.sender,
                                                    packet_data.public_key.toString('hex'));

                /* Search all nicknames */
                for (var key in io.of('/').sockets) {
                    if (packet_data.dest.toLowerCase() === io.of('/').sockets[key].nickname) {
                        socket_id = io.of('/').sockets[key].id;
                    }
                }

                if (socket_id !== null) {
                    /* Each socket is identified by a random,
                    unguessable, unique identifier Socket#id. For your convenience,
                    each socket automatically joins a room identified by its own id. */
                    io.to(socket_id).emit('packet', packet_data);
                }
            break;

            case "secure_2":
                var socket_id = null;

                console.log("[%s]: Public key: %s", packet_data.sender,
                                                    packet_data.public_key.toString('hex'));

                /* Search all nicknames */
                for (var key in io.of('/').sockets) {
                    if (packet_data.dest.toLowerCase() === io.of('/').sockets[key].nickname) {
                        socket_id = io.of('/').sockets[key].id;
                    }
                }

                if (socket_id !== null) {
                    /* Each socket is identified by a random,
                    unguessable, unique identifier Socket#id. For your convenience,
                    each socket automatically joins a room identified by its own id. */
                    io.to(socket_id).emit('packet', packet_data);
                }
            break;

            case "secure_send":
                var socket_id = null;

                console.log("[%s]: Encrypted message: %s\n\tIV: %s", packet_data.sender,
                                                        packet_data.encrypted_msg,
                                                        packet_data.iv.toString('hex'));

                /* Search all nicknames */
                for (var key in io.of('/').sockets) {
                    if (packet_data.dest.toLowerCase() === io.of('/').sockets[key].nickname) {
                        socket_id = io.of('/').sockets[key].id;
                    }
                }

                if (socket_id !== null) {
                    /* Each socket is identified by a random,
                    unguessable, unique identifier Socket#id. For your convenience,
                    each socket automatically joins a room identified by its own id. */
                    io.to(socket_id).emit('packet', packet_data);
                }
            break;

            /* For debugging: kkk; */
            case "kkk":
                console.log(io.of('/'));
            break;

            default:
            break;
        }

    });
});