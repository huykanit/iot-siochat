/*
    The custom event name: packet

    Request in JSON format:
    {sender: name, action: "join", password: what}
    {sender: name, action: "broadcast", msg: msg_content}
    {sender: name, action: "list" }
    {sender: name, action: "quit" }

    {sender: name, action: "send", dest: whom, msg: msg_content}
    {sender: name, action: "cgroup", group: name }
    {sender: name, action: "jgroup", group: name }
    {sender: name, action: "gbroadcast", group: name, msg: msg_content}
    {sender: name, action: "members", group: name }
    {sender: name, action: "msgs", group: name }
    {sender: name, action: "umsgs", user: name }
    {sender: name, action: "groups"}
    {sender: name, action: "leave", group: name }
    {sender: name, action: "invite", group: name, dest: whom}
    {sender: name, action: "kick", group: name, dest: whom, reason: reason}
    {sender: name, action: "ban", group: name, dest: whom, reason: reason}
    {sender: name, action: "unban", group: name, dest: whom}


    The first step of establishing secure connection
    {sender: name, action: "secure_1", dest: whom, public_key: pub_key}
    
    The second step
    {sender: name, action: "secure_2", dest: whom, public_key: pub_key}

    After that, we can send an encrypted message along with iv
    {sender: name, action: "secure_send", dest: whom, encrypted_msg: what, iv: what}
*/

const io = require('socket.io-client'); /* Load 'socket.io-client' module */
const socket = io('http://localhost:3000'); /* Connect to the server */
const readline = require('readline'); /* Load built-in 'readline' module */
const crypto = require('crypto'); /* Load built-in 'crypto' module */
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var nickname = null;
var password = null;

/* Take a look at https://regexr.com/ for testing */
var pattern = /^s;([A-Z0-9]+);(.+)/i; // for sending privately to someone
var pattern_2 = /^bg;([A-Z0-9]+);(.+)/i; // for group broadcast
var pattern_3 = /^sss;([A-Z0-9]+);(.+)/i; // for sending an encrypted message
var pattern_4 = /^invite;([A-Z0-9]+);(.+)/i; // to invite an user into the group
var pattern_5 = /^kick;([A-Z0-9]+);([A-Z0-9]+);(.+)/i; // to kick an user into the
var pattern_ban = /^ban;([A-Z0-9]+);([A-Z0-9]+);(.+)/i; // to ban an user
var pattern_unban = /^unban;([A-Z0-9]+);([A-Z0-9]+)/i; // to unban an user
/* For secure connection */
var ecdh;
var my_public_key, partner_public_key, secret_key;

console.log("Connecting to the server...");

/* Listen to the 'connect' event that fired upon a successfull connection */
socket.on('connect', () => {
    /* All command-line arguments received by the shell are given to the
    process in an array called argv (short for 'argument values')
    Node.js exposes this array for every running process in the form of
    process.argv
    Syntax on command line: node client <nickname> <password> */
    nickname = process.argv[2];
    password = process.argv[3];
    console.log("[INFO]: Welcome %s", nickname);
    socket.emit('packet', {"sender": nickname, "action": "join", "password": password});
});

/* Listen to the 'disconnect' event that fired upon a successfull disconnection */
socket.on('disconnect', (reason) => {
    console.log("[INFO]: Server disconnected, reason: %s", reason);

    /* Close the rl instance. When called, the 'close' event will be emitted */
    rl.close();
});

/* The 'line' event is emitted whenever the input stream receives
an end-of-line input (\n, \r, or \r\n). This usually occurs when
the user presses the <Enter>, or <Return> keys */
rl.on('line', (input) => {
    /* The handler function is called with a string containing
    the single line of received input */

    /* Broadcast a message: b;hello */
    if (true === input.startsWith("b;")) {
        var str = input.slice(2);
        socket.emit('packet', {"sender": nickname, "action": "broadcast",
                                "msg": str});
    }

    /* List all nicknames in the chat: ls; */
    else if ("ls;" === input) {
        socket.emit('packet', {"sender": nickname, "action": "list"});
    }

    /* Quit the chat: q; */
    else if ("q;" === input) {
        socket.emit('packet', {"sender": nickname, "action": "quit"});
    }

    /* Send a message secretly to someone: s;ted;hello ted */
    else if (true === pattern.test(input)) {
        var info = input.match(pattern);
        socket.emit('packet', {"sender": nickname, "action": "send",
                                "dest": info[1], "msg": info[2]});
    }

    /* Create a group: cg;friends */
    else if (true === input.startsWith("cg;")) {
        var str = input.slice(3);
        socket.emit('packet', {"sender": nickname, "action": "cgroup",
                                "group": str});
    }

    /* Join a group: j;friends */
    else if (true === input.startsWith("j;")) {
        var str = input.slice(2);
        socket.emit('packet', {"sender": nickname, "action": "jgroup",
                                "group": str});
    }

    /* Broadcast a message to a group: bg;friends;hello */
    else if (true === pattern_2.test(input)) {
        var info = input.match(pattern_2);
        socket.emit('packet', {"sender": nickname, "action": "gbroadcast",
                                "group": info[1], "msg": info[2]});
    }

    /* List all clients that are inside a group: members;friends */
    else if (true === input.startsWith("members;")) {
        var str = input.slice(8);
        socket.emit('packet', {"sender": nickname, "action": "members",
                                "group": str});
    }

    /* List the history of messages exchanged in a group:
    messages;friends */
    else if (true === input.startsWith("messages;")) {
        var str = input.slice(9);
        socket.emit('packet', {"sender": nickname, "action": "msgs",
                                "group": str});
    }

    /* List the history of messages belonging to a user:
    umessages;ted */
    else if (true === input.startsWith("umessages;")) {
        var str = input.slice(10);
        socket.emit('packet', {"sender": nickname, "action": "umsgs",
                                "user": str});
    }

    /* List the existing groups: groups; */
    else if ("groups;" === input) {
        socket.emit('packet', {"sender": nickname, "action": "groups"});
    }

    /* Leave a group: leave;friends */
    else if (true === input.startsWith("leave;")) {
        var str = input.slice(6);
        socket.emit('packet', {"sender": nickname, "action": "leave",
                                "group": str});
    }

    // Invite a user into specified group
    else if (pattern_4.test(input) === true) {
        var info = input.match(pattern_4);
        socket.emit("packet",
            {
                "sender": nickname,
                "action": "invite",
                "group": info[1],
                "dest": info[2]
            });
    }

    // Kick a user into specified group
    else if (pattern_5.test(input) === true) {
        var info = input.match(pattern_5);
        socket.emit("packet",
            {
                "sender": nickname,
                "action": "kick",
                "group": info[1],
                "dest": info[2],
                "reason": info[3]
            });
    }

    // Ban an user
    else if (pattern_ban.test(input) === true) {
        var info = input.match(pattern_ban);
        socket.emit("packet",
            {
                "sender": nickname,
                "action": "ban",
                "group": info[1],
                "dest": info[2],
                "reason": info[3]
            });
    }

    // Un ban an user
    else if (pattern_unban.test(input) === true) {
        var info = input.match(pattern_unban);
        socket.emit("packet",
            {
                "sender": nickname,
                "action": "unban",
                "group": info[1],
                "dest": info[2],
            });
    }

    /* Establish a secure connection with Ted: ss;ted */
    // Share key: https://asecuritysite.com/encryption/js_ecdh
    else if (true === input.startsWith("ss;")) {
        var str = input.slice(3);

        // Create an ecdh instance with key size of 256 bits
        ecdh = crypto.createECDH('secp256k1');

        // Generates private and public key, and returns the public key
        my_public_key = ecdh.generateKeys();

        socket.emit('packet', {"sender": nickname, "action": "secure_1",
                                "dest": str, "public_key": my_public_key});
    }

    /* Send an encrypted message to Ted: sss;ted;hello ted */
    else if (true === pattern_3.test(input)) {
        var info = input.match(pattern_3);

        // Length of secret key is dependent on the algorithm. In this case for aes256,
        // it requires 32 bytes (256 bits), please pay attention that our secret key
        // generated in the establish steps meet this requirement
        const algorithm = 'aes-256-cbc';

        // Initialization vector
        // Block sizes of AES are 128 bits, iv has the same size as the block size
        const randome_iv = crypto.randomBytes(16);

        // Create cipher object with the given arguments
        const cipher = crypto.createCipheriv(algorithm, secret_key, randome_iv);

        // Start to encrypt
        let encrypted = cipher.update(info[2] /* raw message */, 'utf8', 'hex');
        encrypted += cipher.final('hex');

        // Send iv along with the encrypted message for decrypting message
        socket.emit('packet', {"sender": nickname, "action": "secure_send",
                                "dest": info[1], "encrypted_msg": encrypted, "iv": randome_iv});
    }

    

    /* For debugging: kkk; */
    else if ("kkk;" === input) {
        socket.emit('packet', {"sender": nickname, "action": "kkk"});
    }

    else {
        console.log("[ERROR]: don't support this kind of syntax");
    }
});

/* Listen to the 'packet' event that is emitted from the server */
socket.on('packet', (packet_data) => {
    switch (packet_data.action) {
        case "join":
            console.log("[INFO]: %s has joined the chat", packet_data.sender);
        break;

        case "broadcast":
            console.log("%s", packet_data.msg);
        break;

        case "list":
            console.log("[INFO]: List of nicknames:");
            for (var i = 0; i < packet_data.users.length; i++) {
                console.log(packet_data.users[i]);
            }
        break;

        case "quit":
            console.log("[INFO]: %s quit the chat", packet_data.sender);
        break;

        case "send":
            console.log("%s", packet_data.msg);
        break;

        case "cgroup":
            // console.log("need?");
            console.log("[INFO]: The group %s has been created", packet_data.group);
        break;

        case "jgroup":
            console.log("[INFO]: %s has joined the group", packet_data.sender);
        break;

        case "gbroadcast":
            console.log("%s", packet_data.msg);
        break;

        case "members":
            console.log("[INFO]: List of members:");
            for (var i = 0; i < packet_data.members.length; i++) {
                console.log(packet_data.members[i]);
            }
        break;

        case "msgs":
            console.log("[INFO]: History of messages in group", packet_data.group);
            for (var i = 0; i < packet_data.msgs.length; i++) {
                console.log(packet_data.msgs[i]);
            }
        break;

        case "umsgs":
            console.log("[INFO]: History of messages belong to", packet_data.user);
            for (var i = 0; i < packet_data.msgs.length; i++) {
                console.log(packet_data.msgs[i]);
            }
        break;

        case "groups":
            console.log("[INFO]: List of groups:");
            for (var i = 0; i < packet_data.groups.length; i++) {
                console.log(packet_data.groups[i]);
            }
        break;

        case "leave":
            console.log("[INFO]: %s left the group", packet_data.sender);
        break;

        case "invite":
            console.log("[INFO] User <%s> has been added into group <%s>", packet_data.dest, packet_data.group);
            break;

        case "kick":
            console.log("[INFO] User <%s> has been removed out of the group <%s>, reason: %s", packet_data.dest, packet_data.group, packet_data.reason);
            break;

        case "ban":
            console.log("[INFO] User <%s> has been ban from group <%s>, reason: %s", packet_data.dest, packet_data.group, packet_data.reason);
            break;

        case "unban":
            console.log("[INFO] User <%s> has been unban from group <%s>, reason: %s", packet_data.dest, packet_data.group, packet_data.reason);
            break;

        case "secure_1":
            partner_public_key = packet_data.public_key;

            // Create an ecdh instance with key size of 256 bits
            ecdh = crypto.createECDH('secp256k1');

            // Generates private and public key, and returns the public key
            my_public_key = ecdh.generateKeys();

            // Send my public key back to the sender
            socket.emit('packet', {"sender": nickname, "action": "secure_2",
                                    "dest": packet_data.sender, "public_key": my_public_key});
            // Generates the secret key, please pay attention that this key is the same at two ends
            secret_key = ecdh.computeSecret(partner_public_key);

            // For debugging
            // console.log("[INFO]: Secret key: %s", secret_key.toString('hex'));
        break;

        case "secure_2":
            partner_public_key = packet_data.public_key;

            // Generates the secret key, please pay attention that this key is the same at two ends
            secret_key = ecdh.computeSecret(partner_public_key);
            
            // For debugging
            // console.log("[INFO]: Secret key: %s", secret_key.toString('hex'));
        break;

        case "secure_send":
            // Use the same algorithm as the sender
            const algorithm = 'aes-256-cbc';

            // Initialization vector from the sender
            const iv = packet_data.iv;

            // Create decipher object with the given arguments
            const decipher = crypto.createDecipheriv(algorithm, secret_key, iv);

            // Start to decrypt the message from the sender
            let decrypted = decipher.update(packet_data.encrypted_msg, 'hex', 'utf8');
            decrypted += decipher.final('utf8');

            // Show the decrypted message at our end
            console.log("%s", decrypted);
        break;

        default:
        break;
    }
});