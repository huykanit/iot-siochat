var expect = require('chai').expect;
const io = require('socket.io-client'); /* Load 'socket.io-client' module */
var tests = 1;

describe('---- Testing Server in the chat ----', function() {

    var socket_1, socket_2;
    var nickname_1 = "ted";
    var nickname_2 = "roni";
    var options = { 'force new connection': true };

    // Runs before each test in this block
    beforeEach(function (done) {
        // Assuming a server is listening for socket connections
        // Connect to the server
        socket_1 = io('http://localhost:3000', options);
        socket_2 = io('http://localhost:3000', options);

        console.log(">> Test #" + (tests++));
        socket_1.on('connect', function() {
            console.log("socket_1 connected...");
            socket_1.emit('packet', {"sender": nickname_1, "action": "join", "password": nickname_1});
        });

        socket_2.on('connect', function() {
            console.log("socket_2 connected...");
            socket_2.emit('packet', {"sender": nickname_2, "action": "join",  "password": nickname_2});
        });

        socket_1.on('disconnect', function() {
            console.log("socket_1 disconnected");
        });

        socket_2.on('disconnect', function() {
            console.log("socket_2 disconnected\n");
        });

        // The done() callback must be called for Mocha to terminate the test
        // and proceed to the next test, otherwise the test keeps running
        // until the timeout reaches.
        done();
    });

    // Runs after each test in this block
    afterEach(function (done) {

        // Disconnect io clients
        socket_1.disconnect();
        socket_2.disconnect();

        // The done() callback must be called for Mocha to terminate the test
        // and proceed to the next test, otherwise the test keeps running
        // until the timeout reaches.
        done();
    });

    it('Notify that a user joined the chat', function(done) {
        // socket_1.emit('packet', {"sender": nickname_1, "action": "join"});

        socket_2.on('packet', function(packet_data) {
            expect(packet_data.sender).to.equal(nickname_1);
            expect(packet_data.action).to.equal("join");
            done();
        });
    });
    
    it('List all users in the chat', function(done) {
        socket_1.emit('packet', {"sender": nickname_1, "action": "list"});
        socket_1.on('packet', function(packet_data) {
            if ("list" === packet_data.action) {
                expect(packet_data.users).to.be.an('array').that.includes(nickname_1);
                expect(packet_data.users).to.be.an('array').that.includes(nickname_2);
                done();
            }
        });
    });

    

    it('Broadcast a message to others in the chat', function(done) {
        var msg_hello = "hello socket_2";
        socket_1.emit('packet', {"sender": nickname_1, "action": "broadcast",
                                "msg": msg_hello});
        socket_2.on('packet', function(packet_data) {
            if ("broadcast" === packet_data.action) {
                expect(packet_data.msg).to.equal(msg_hello);
                done();
            }
        });
    });

    it('A user quit the chat', function(done) {
        socket_1.emit('packet', {"sender": nickname_1, "action": "quit"});
        socket_2.on('packet', function(packet_data) {
            if ("quit" === packet_data.action) {
                expect(packet_data.sender).to.equal(nickname_1);
                done();
            }
        });
    });

    it('Notify that a user quit the chat', function(done) {
        socket_1.emit('packet', {"sender": nickname_1, "action": "quit"});
        socket_2.on('packet', function(packet_data) {
            if ("quit" === packet_data.action) {
                expect(packet_data.sender).to.equal(nickname_1);
                done();
            }
        });
    });

    it('Send a message secretly to someone', function(done) {
        var msg_hello = "hello socket_2";
        socket_1.emit('packet', {"sender": nickname_1, "action": "send",
                                "dest": nickname_2, "msg": msg_hello});
        socket_2.on('packet', function(packet_data) {
            if ("send" === packet_data.action) {
                expect(packet_data.dest).to.equal(nickname_2);
                expect(packet_data.msg).to.equal(msg_hello);
                done();
            }
        });
    });
});
