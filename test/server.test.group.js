var expect = require('chai').expect;
const io = require('socket.io-client'); /* Load 'socket.io-client' module */
var tests = 1;

describe('---- Testing Server in a group ----', function() {

    var socket_1, socket_2, socket_3;
    var options = { 'force new connection': true };
    var nickname_1 = "ted";
    var nickname_2 = "roni";
    var group_name = "friends";

    // Runs before each test in this block
    beforeEach(function (done) {
        // Assuming a server is listening for socket connections
        // Connect to the server
        socket_1 = io('http://localhost:3000', options);
        socket_2 = io('http://localhost:3000', options);

        console.log(">> Test #" + (tests++));
        socket_1.on('connect', function() {
            console.log("socket_1 connected...");
            socket_1.emit('packet', {"sender": nickname_1, "action": "join",  "password": nickname_1});
            socket_1.emit('packet', {"sender": nickname_1, "action": "jgroup",
                                    "group": group_name});
        });

        socket_2.on('connect', function() {
            console.log("socket_2 connected...");
            socket_2.emit('packet', {"sender": nickname_2, "action": "join",  "password": nickname_2});
            socket_2.emit('packet', {"sender": nickname_2, "action": "jgroup",
                                    "group": group_name});

            // Need some time for socket_2 join a group,
            // so wait for 1s before calling done() function
            // setTimeout(() => {  done(); }, 1000);
        });

        socket_1.on('disconnect', function() {
            console.log("socket_1 disconnected");
        });

        socket_2.on('disconnect', function() {
            console.log("socket_2 disconnected\n");
        });

        // The done() callback must be called for Mocha to terminate the test
        // and proceed to the next test, otherwise the test keeps running
        // until the timeout reaches.
        done();
    });

    // Runs after each test in this block
    afterEach(function (done) {

        // Disconnect io clients
        socket_1.disconnect();
        socket_2.disconnect();

        // The done() callback must be called for Mocha to terminate the test
        // and proceed to the next test, otherwise the test keeps running
        // until the timeout reaches.
        done();
    })

    it('List all clients that are inside a group', function(done) {
        socket_1.emit('packet', {"sender": nickname_1, "action": "members",
                                        "group": group_name});

        setTimeout(() => {}, 100);
        socket_1.on('packet', function(packet_data) {
            if ("members" === packet_data.action) {
                expect(packet_data.group).to.equal(group_name);
                expect(packet_data.members).to.be.an('array').that.includes(nickname_1);
                done();
            }
        });
    });

    // it('Broadcast a message to a group', function(done) {
    //     var msg_hello = "hello socket_2";

    //     socket_1.emit('packet', {"sender": nickname_1, "action": "gbroadcast",
    //                             "group": group_name, "msg": msg_hello});
    //     setTimeout(() => {}, 1000);
    //     socket_2.on('packet', function(packet_data) {
    //         console.log(packet_data);
    //         if ("gbroadcast" === packet_data.action) {
    //             expect(packet_data.group).to.equal(group_name);
    //             expect(packet_data.msg).to.equal(msg_hello);
    //             done();
    //         }
    //     });
    // });


    it('Notify that a user joined a group', function(done) {
        socket_1.on('packet', function(packet_data) {
            if ("jgroup" === packet_data.action) {
                expect(packet_data.sender).to.equal(nickname_2);
                expect(packet_data.group).to.equal(group_name);
                done();
            }
        });
    });

    it('Ban user from group', function(done) {
        socket_1.emit('packet', {sender: nickname_1, action: "ban", group: group_name, dest: nickname_2, reason: "test"});
        setTimeout(() => {  done(); }, 10);
        socket_2.on('packet', function(packet_data) {
            if ("ban" === packet_data.action) {
                expect(packet_data.group).to.equal(group_name);
                expect(packet_data.dest).to.equal(nickname_2);
                done();
            }
        });
    });

    it('Un ban user from group', function(done) {
        socket_1.emit('packet', {sender: nickname_1, action: "ban", group: group_name, dest: nickname_2, reason: "test"});
        setTimeout(() => {  done(); }, 10);
        socket_1.emit('packet', {sender: nickname_1, action: "unban", group: group_name, dest: nickname_2});
        socket_2.on('packet', function(packet_data) {
            if ("unban" === packet_data.action) {
                expect(packet_data.group).to.equal(group_name);
                expect(packet_data.dest).to.equal(nickname_2);
                done();
            }
        });
    });

    it('List the existing groups', function(done) {
        socket_1.emit('packet', {"sender": nickname_1, "action": "groups"});
        setTimeout(() => {  done(); }, 10);
        socket_1.on('packet', function(packet_data) {
            if ("groups" === packet_data.action) {
                expect(packet_data.groups).to.be.an('array').that.includes(group_name);
                done();
            }
        });
    });

});